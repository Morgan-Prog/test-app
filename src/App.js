import React, { useState } from 'react';
// import logo from './logo.svg';
import './App.css';

import { Layout, Menu, Breadcrumb } from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';

import { BrowserRouter as Router, Route, Switch, useHistory } from 'react-router-dom';
import Page from './Page';
import About from './About';

//import au dessus de const

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;






const App = () => {

  const onCollapse = () => {
    console.log(collapsed);
    setCollapsed(!collapsed)
  };

  const history = useHistory(); // initialiser constante du routing

  const [collapsed, setCollapsed] = useState(true) //etat par défaut de la sidebar
  return (

    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
        <div className="logo" />
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1" icon={<PieChartOutlined />} onClick={() => history.push('/About')}>
            Option 1
            </Menu.Item>
          <Menu.Item key="2" icon={<DesktopOutlined />} onClick={() => history.push("/")}>
            Option 2
            </Menu.Item>
          <SubMenu key="sub1" icon={<UserOutlined />} title="User">
            <Menu.Item key="3">Tom</Menu.Item>
            <Menu.Item key="4">Bill</Menu.Item>
            <Menu.Item key="5">Alex</Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
            <Menu.Item key="6">Team 1</Menu.Item>
            <Menu.Item key="8">Team 2</Menu.Item>
          </SubMenu>
          <Menu.Item key="9" icon={<FileOutlined />} />
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }} />
        <Switch>
          <Route exact component={About} path='/About'></Route>
          <Route component={Page} path="/"></Route>
        </Switch>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
    </Layout>
  );




}

export default App;
